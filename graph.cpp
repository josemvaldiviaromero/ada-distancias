#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <chrono>
#include <ctime>
#include <algorithm>
#include <sstream>
#include <fstream>

using namespace std;

template <class t>
string nToString(t number)
{
    ostringstream ss;
    ss << number;
    return ss.str();
}

template<typename T>
class Node{

public:

	T data;
	vector < pair <Node<T> *, T > > edges;
	int edges_out;

	Node()
	{
		edges_out=0;
	}
	Node( T data ){
		this->data=data;
		edges_out=0;
	}
	~Node(){}

	void ShowEdges()
	{
		for (typename std::vector<pair<Node<T>*, T> >::iterator i = edges.begin(); i != edges.end(); ++i)
		{
			cout<< " --> ";
			cout<< ((*i).first)->data ;
			cout<< "(" << ((*i).second) << ")";
		}
	}

private:


};

template<typename T>
class Graph{
public:

	vector< Node<T>* > nodes;
	int nodeqty;
	int edgeqty;

	Graph(){
		nodeqty=0;
		edgeqty=0;

	}

	~Graph(){
		
		for (typename std::vector< Node<T>* > ::iterator i = nodes.begin(); i != nodes.end(); ++i)
		{
			delete *i;
		}
	}

	bool ExistsNode(T data){

		for (typename std::vector< Node<T>* > ::iterator i = nodes.begin(); i != nodes.end(); ++i)
		{
			if ((*i)->data== data)
			{
				return true;
			}
		}
		return false;
	}

	bool AddNode(T data){

		if (!ExistsNode(data)){

			Node<T>* tmp;
			tmp= new Node<T> (data);
			nodes.push_back(tmp);
			nodeqty++;
			return true;
		}
		return false;
	}

	Node<T> * SearchNode(T data){

		for (typename std::vector <Node<T>* >::iterator i = nodes.begin(); i != nodes.end(); ++i)
		{
			if ((*i)->data== data)
			{
				return (*i);
			}
			
		}
		return NULL;
	}

	bool AddEdge (T data_from, T data_to, T weight){

		Node<T> * from = SearchNode(data_from);
		Node<T> * to = SearchNode(data_to);
		if(from != NULL and to != NULL){
			pair < Node <T> * , T > tmp;
			tmp.first= to;
			tmp.second = weight;
			from -> edges. push_back(tmp);
			from->edges_out++;
			edgeqty++;
			return true;
		}
		return false;

	}

	void ShowGraph(){
		for (typename std::vector< Node<T> * >::iterator i = nodes.begin(); i != nodes.end(); ++i)
		{
			cout<< (*i)->data << " ";
			(*i)->ShowEdges();
			cout<<"\n\n";
		}

	}

	void UploadData(char* file)
	{
		int q_nodes;
		int q_edges;
		ifstream data(file);
		data>>q_nodes;
		data>>q_edges;
		while (data)
		{
			int tmp_node;
			int tmp_dir;
			int tmp_w;
			data >>tmp_node;
			data >>tmp_dir;
			data >>tmp_w;
			AddNode(tmp_node);
			AddEdge(tmp_node,tmp_dir,tmp_w);

		}		

	}


	T min (T data_first, T data_second)
	{
		if (data_first<=data_second)
		{
			return data_first;
		}
		return data_second;

	}
/* DEPRECATED
	T* BellManFord(T data)
	{

		T* bf_table = new T [nodeqty+1];

		bf_table[data]=0;
		for (int i=1; i<=nodeqty;i++)
		{
			if(data != i)
			{
				bf_table[i]=numeric_limits<T>::infinity();
			}

		}

		for (int i = 1; i < nodeqty-1; ++i)
		{
			Node<T> * tmp = SearchNode(i);
			
			for (typename std::vector<pair<Node<T>*, T> >::iterator a = tmp->edges.begin(); a != tmp->edges.end(); ++a)
			{
				bf_table[((*a).first)->data]=min(bf_table[((*a).first)->data], ((*a).second)+bf_table[i]);
								
			}

		}

		return bf_table;
	}

	*/

	T** FloydWarshall ()
	{
		T** dist = new T* [nodeqty+1];
		for (int i=0;i<nodeqty+1;i++)
		{
			dist[i]=new T [nodeqty+1];
		}

		for(int i=0;i<=nodeqty;i++)
		{
			for(int j=0;j<=nodeqty;j++)
			{
				dist[i][j]=numeric_limits<T>::infinity();
			
			}

		}

		for (int i=1;i<=nodeqty;i++)
		{
			dist[i][i]=0;
		}

		for (typename std::vector<Node<T>*>::iterator i = nodes.begin(); i != nodes.end(); ++i)
		{
			for (typename std::vector<pair<Node<T>* , T> >::iterator e = (*i)->edges.begin(); e != (*i)->edges.end(); ++e)
			{
				
				dist[(int)((*i)->data)][((int)((*e).first)->data)]=(*e).second;

			}
		}

		for(int k=1;k<=nodeqty;k++)
		{
			cout<<"K: "<< k<<endl;
			for(int i =1; i<=nodeqty;i++)
			{
				for (int j=1;j<=nodeqty;j++)
				{
					

					if (dist[i][j]>(dist[i][k]+dist[k][j]))
					{
						dist[i][j]=dist[i][k]+dist[k][j];
					}
				}
			}
		}

		return dist;

	}
	
protected:
private:
};


pair<double,double> min (double * data,int size,int dat)
{
	double mini=1000000;
	int a=1;
	for (int i=1;i<=size;i++ )
	{
		
		if (mini>data[i] and dat!=i)
		{
			mini =data[i];
			a=i;
		}
	}
	pair<int,int> res;
	res.first= a;
	res.second=(int)mini;
	return res;
}

pair<double,double> max (double * data,int size,int dat)
{
	double maxi=-100000;
	int a=1;
	for (int i=1;i<=size;i++ )
	{
		if (maxi<data[i] and dat!=i and data[i]!=numeric_limits<double>::infinity())
		{
			maxi =data[i];
			a=i;
		}
	}
	
	pair<int,int> res;
	res.first= (double)a;
	res.second=(int)maxi;
	return res; 
}

pair<double,pair<double,double> > GetMin(pair<double,double>* data,int size)
{
	pair<double,double> min;
	min.first=0;
	min.second=1000000;
	int a=1;
	for (int i=1;i<=size;i++)
	{
		if(min.second>data[i].second)
		{
			min.first=data[i].first;
			min.second=data[i].second;
			a=i;
		}

	}
	pair< double, pair <double,double>  > res;
	res.second.first = min.first;
	res.second.second = min.second;
	res.first= a;

	return res;
}

pair<double, pair<double,double> > GetMax(pair<double,double>* data,int size)
{
	pair<double,double> max;
	max.first=0;
	max.second=-100000;
	int a=1;
	for (int i=1;i<=size;i++)
	{

		if(max.second<data[i].second)
		{
			max.first=data[i].first;
			max.second=data[i].second;
			a=i;
		}

	}

	pair< double, pair <double,double>  > res;
	res.second.first = max.first;
	res.second.second=max.second;
	res.first= a;
	return res;
}

int main(){

	Graph <double> a;
	
	// a.UploadData("GrafoTrab4.txt");
	a.UploadData("test_graph");
	
	
	/*a.AddNode(1);
	a.AddNode(2);
	a.AddNode(3);
	a.AddNode(4);
	a.AddEdge(1,3,-2);
	a.AddEdge(2,1,4);
	a.AddEdge(2,3,3);
	a.AddEdge(3,4,2);
	a.AddEdge(4,2,-1);*/
	cout<<"Data Uploaded"<<endl;

	using namespace std::chrono;
	
	//int * tmp= a.BellManFord(1);
	steady_clock::time_point start = steady_clock::now();
    		
			
	double ** tmp = a.FloydWarshall();
	for (int i=1;i<=a.nodeqty;i++)
	{
		for (int j=1;j<=a.nodeqty;j++)
		{
			cout<<tmp[i][j]<<"\t";
		}
		cout<<endl;
	}
	pair <double,double> * resmin= new pair<double,double> [a.nodeqty+1];
	pair <double,double> * resmax= new pair<double,double> [a.nodeqty+1];
	for (int i =1;i<=a.nodeqty;i++)
	{

		resmin[i]=min(tmp[i],a.nodeqty,i);
		resmax[i]=max(tmp[i],a.nodeqty,i);

	}

	pair<double,pair<double,double> > max=GetMax(resmax,a.nodeqty);
	pair<double, pair<double,double> >  min=GetMin(resmin,a.nodeqty);

	steady_clock::time_point end = steady_clock::now();
	
	duration<double> time_span = duration_cast<duration<double>>(end - start);
	

	for(int i=0; i<=a.nodeqty;i++)
	{
		// cout<<"s:"<<resmax[i].second<<endl;
	}

	/*cout<<"NODE: "<<max.first<< " MAXIMUM: " <<max.second.first << " WEIGHT: " <<max.second.second<<endl;
	cout <<"NODE: "<<min.first <<" MINIMUM: " <<min.second.first << " WEIGHT: " <<min.second.second<<endl;
	cout<<"Demoro: " <<time_span.count()<<"segundos";*/

	// Saving results
	string results = "";
	results += "NODE: " + nToString(max.first) + " MAXIMUM: " + nToString(max.second.first) + " WEIGHT: " + nToString(max.second.second) + "\n";
	results += "NODE: " + nToString(min.first) + " MINIMUM: " + nToString(min.second.first) + " WEIGHT: " + nToString(min.second.second) + "\n";
	results += "Demoro: " + nToString(time_span.count()) + " segundos";

	ofstream resultsFile("results");
	cout << results << endl;
	resultsFile << results;


	for (int i=0;i<=a.nodeqty;i++)
	{
		delete [] tmp[i];
	}
	delete [] tmp;

	

	//a.ShowGraph();
	return 0;
}
