#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <sstream>
#include <fstream>

using namespace std;

template <class t>
string nToString(t number)
{
    ostringstream ss;
    ss << number;
    return ss.str();
}

int main(){

	long nNodes = 600;
	long nEdges = nNodes*5;
	string result = "";
	ofstream test_graph("test_graph");
	
	srand (time(NULL));

	result += nToString(nNodes) + " " + nToString(nEdges) + "\n";
	for(long i=0;i<nNodes;i++){
		for(long j=0;j<(rand()%20+5);j++){
			long targetNode = rand()%nNodes+1;
			while(targetNode == (i+1)) targetNode = rand()%nNodes+1;
			result += nToString(i+1) + " " + nToString(targetNode) + " " + nToString(rand()%50+1) + "\n";
		}
	}

	cout << result << endl;
	test_graph << result;

	return 0;
}
